var express = require("express");
var app = express();

var bodyParser = require('body-parser');
var serveStatic = require('serve-static');
const got = require('got');
const FileType = require('file-type');

app.use(serveStatic('public', { 'index': ['default.html', 'default.htm'] }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



//Testing service removeduplicatewords

app.post("/removeduplicatewords", function(req, res) {
    var wordsList = [];
    var inputObj = (req.body.palabras);
    var input = inputObj.split(",");

    for (var i = 0; i < input.length; ++i) {
        if (!wordsList.includes(input[i])) {
            wordsList.push(input[i])
        }
    }
    res.send(wordsList.toString());
});

//Testing service botorder
var bot = null;

app.get("/botorder/:order", function(req, res) {
    var order = req.params.order;
    if (bot == null) {
        res.end("NONE");
    }
    else {
        res.end(bot);
    }
});

app.post("/botorder/order1", function(req, res) {
    var result = req.body.botorder;
    bot = result;
    res.end("OK");
});

/*
//Testing service detectfilytype

app.post("/detectfiletype", function(req, res) {
    var url = (req.body.url);

    (async() => {
        const stream = got.stream(url);
        res.end(JSON.stringify(await FileType.fromStream(stream)));
        //=> {ext: 'jpg', mime: 'image/jpeg'}
    })();
});

var jsondft = { "url": "https://cdn02.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/H2x1_NSwitch_FireEmblemThreeHouses_image1600w.jpg"};

request.post(
    SERVERURL+'/detectfiletype',
    { json:  jsondft },
    function (error, response, body) {
    	console.log("Testing: detectfiletype");
        if (!error && response.statusCode == 200) {
        	console.log("Result: "+JSON.stringify(body));
        	if(JSON.stringify(body)=="{\"ext\":\"png\",\"mime\":\"image/png\"}"){
        		console.log("detectfiletype = OK");
        	}else{
        		console.log("detectfiletype = NOT OK ");
        	}
        }else{
        	console.log("detectfiletype: ERROR: "+error);
        }
        console.log("##############################");
    }
);

});*/

app.use(function(req,res){
    res.status(404);
    res.send("default 404 webpage");
});

app.listen(8000);